export const environment = {
  production: true,
  API_URL: 'http://agora-rest-api.herokuapp.com/api/v1'
};
